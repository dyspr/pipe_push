var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var numOfCirlces = 97
var columns = 6
var initSize = 0.045
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < columns; j++) {
    push()
    translate(windowWidth * 0.5 + (j - Math.floor(columns * 0.5) + 0.5) * boardSize * initSize * 3, 0)
    for (var i = 0; i < numOfCirlces; i++) {
      if (Math.floor((i * 10 + frame * 19 + j * 9) % 9) === 0) {
        fill(255)
      } else {
        fill(0)
      }
      stroke(255)
      strokeWeight(2 * abs(sin(frame + i * 0.1 * (1 / numOfCirlces))))
      push()
      translate(0.5 * initSize * boardSize * sin(frame + i * 0.5 + j * Math.PI), windowHeight * 0.5 + (i - Math.floor(numOfCirlces * 0.5)) * initSize * boardSize * 0.2)
      ellipse(0, 0, boardSize * initSize)
      pop()
    }
    pop()
  }

  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
